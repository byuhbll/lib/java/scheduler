# Scheduler

The Scheduler is a light-weight, but powerful task scheduler based on cron expressions, periodic timers, single action timers, or any other kind of scheduling. A single Scheduler can be created for an entire application. Scheduling occurs in a single thread, but tasks run in their own Executor. By default, scheduled tasks are designed to be self-throttling. For cron tasks this means that if the task is running when a timeout should occur, that execution gets dropped. For periodic tasks this means that the next timeout is not scheduled until the currently running execution is completed. These throttling defaults are adjustable. See the javadocs at http://javadoc.io/doc/edu.byu.hbll/scheduler for more information.

## Getting Started

Add this library as a dependency to your project's pom.xml.

```xml
<dependency>
  <groupId>edu.byu.hbll</groupId>
  <artifactId>scheduler</artifactId>
  <version>1.1.0</version>
</dependency>
```

## Documentation

See http://javadoc.io/doc/edu.byu.hbll/scheduler for more documentation.

### Examples

One time task. This will print the current time one second after being scheduled.

```java
Scheduler scheduler = new Scheduler();
System.out.println(LocalDateTime.now() + " start");
scheduler.newOneTimeTask(Duration.ofSeconds(1), () -> System.out.println(LocalDateTime.now()));
Thread.sleep(2000);
scheduler.shutdown();
scheduler.awaitTermination(Duration.ofMinutes(1));
System.out.println(LocalDateTime.now() + " end");
```

Periodic task. This will print the current time immediately and then every second after that for ten seconds.

```java
Scheduler scheduler = new Scheduler();
System.out.println(LocalDateTime.now() + " start");
scheduler.schedulePeriodicTask(Duration.ofSeconds(1), () -> System.out.println(LocalDateTime.now()));
Thread.sleep(10000);
scheduler.shutdown();
scheduler.awaitTermination(Duration.ofMinutes(1));
System.out.println(LocalDateTime.now() + " end");
```

Cron task. This will print the current time every second on the second for ten seconds.

```java
Scheduler scheduler = new Scheduler();
System.out.println(LocalDateTime.now() + " start");
scheduler.scheduleCronTask("* * * * * * *", () -> System.out.println(LocalDateTime.now()));
Thread.sleep(10000);
scheduler.shutdown();
scheduler.awaitTermination(Duration.ofMinutes(1));
System.out.println(LocalDateTime.now() + " end");
```

Periodic task with all options. This will print the current time immediately and then every second after that for ten seconds. The timeout will be offset by 100ms and up to 100ms up or down of random variation. Some options set have no real effect. They are just to show the different available options.

```java
Scheduler scheduler = new Scheduler();
System.out.println(LocalDateTime.now() + " start");
Schedule schedule =
    new PeriodicSchedule.Builder(Duration.ofSeconds(1))
        .delay(Duration.ofSeconds(1))
        .asynchronous(true)
        .preserve(true)
        .fixedRate(true)
        .jitter(Duration.ofMillis(100))
        .offset(Duration.ofMillis(100))
        .build();
scheduler.scheduleTask(schedule, () -> System.out.println(LocalDateTime.now()));
Thread.sleep(10000);
scheduler.shutdown();
scheduler.awaitTermination(Duration.ofMinutes(1));
System.out.println(LocalDateTime.now() + " end");
```

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally in order to include it in another project for testing, run the following from the project directory on the host machine.

```
docker run -it -u $UID:$(id -g) -e MAVEN_CONFIG=/var/maven/.m2 -v ~/.m2:/var/maven/.m2 -v .:/project maven mvn -f /project/pom.xml -Duser.home=/var/maven clean package install
```

Building and deploying this project is handled by the GitLab pipeline. There are two possible destinations for the built project: the BYU HBLL internal Maven repository or the central Maven repository. Use the internal repository for non-public releases and the central repository for public open source releases. To instruct the pipeline to deploy to Maven Central, set the `MAVEN_CENTRAL_DEPLOY` CI/CD variable to `true`. Deploying to either the internal or central repository requires manually playing the deploy job. Note: Deployments to Maven Central are permanent.

## License

[License](LICENSE.md)