package edu.byu.hbll.scheduler;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.AfterEach;

/** Set up and helper methods for various test classes. */
public class SchedulerTesting {

  protected Scheduler scheduler = new Scheduler();
  protected Counter counter = new Counter(0);
  protected ScheduledTask task = scheduler.newOneTimeTask(Duration.ZERO, counter);

  @AfterEach
  public void after() {
    scheduler.shutdown();
  }

  protected boolean sleep(long millis) {
    try {
      Thread.sleep(millis);
    } catch (InterruptedException e) {
      return false;
    }

    return true;
  }

  protected class Counter implements Runnable {
    final AtomicInteger count = new AtomicInteger();
    long delay = 0;

    protected Counter(long delay) {
      this.delay = delay;
    }

    @Override
    public void run() {
      if (sleep(delay)) {
        count.incrementAndGet();
      }
    }

    protected int get() {
      return count.get();
    }
  }
}
