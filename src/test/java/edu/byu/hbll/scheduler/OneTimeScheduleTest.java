package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import org.junit.jupiter.api.Test;

public class OneTimeScheduleTest extends SchedulerTesting {

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.OneTimeSchedule#next(edu.byu.hbll.scheduler.ScheduledTask,
   * java.time.Instant, java.time.Instant)}.
   */
  @Test
  public void testNextScheduledTaskInstantInstant() {
    counter = new Counter(0);
    ScheduledTask task = scheduler.newOneTimeTask(Duration.ZERO, counter);
    sleep(100);
    assertEquals(1, counter.get());
    assertTrue(task.isDone());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.OneTimeSchedule#builder()}. */
  @Test
  void testBuilder() {
    assertTrue(OneTimeSchedule.builder() instanceof OneTimeSchedule.Builder);
    assertTrue(OneTimeSchedule.builder().build() instanceof OneTimeSchedule);
  }
}
