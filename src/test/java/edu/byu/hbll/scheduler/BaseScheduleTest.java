package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class BaseScheduleTest extends SchedulerTesting {

  @Mock ScheduledTask mockedTask;

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.BaseSchedule#BaseSchedule(edu.byu.hbll.scheduler.BaseSchedule.Builder)}.
   */
  @Test
  public void testBaseSchedule() {
    assertNotNull(new OneTimeSchedule.Builder().build());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.BaseSchedule#next(edu.byu.hbll.scheduler.ScheduledTask)}.
   */
  @Test
  public void testNextScheduledTaskAysnchronous() {
    Schedule schedule =
        new PeriodicSchedule.Builder(Duration.ofMillis(50)).asynchronous(true).build();
    counter = new Counter(100);
    scheduler.scheduleTask(schedule, counter);
    sleep(75);
    assertEquals(0, counter.get());
    sleep(50);
    assertEquals(1, counter.get());
    sleep(50);
    assertEquals(2, counter.get());
    sleep(50);
    assertEquals(3, counter.get());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.BaseSchedule#next(edu.byu.hbll.scheduler.ScheduledTask)}.
   */
  @Test
  public void testNextScheduledTaskPreserve() {
    lenient().when(mockedTask.getCreationTime()).thenReturn(Instant.EPOCH);
    lenient().when(mockedTask.getNumScheduled()).thenReturn(0l).thenReturn(1l).thenReturn(2l);
    lenient()
        .when(mockedTask.getNext())
        .thenReturn(Instant.EPOCH.plusMillis(1))
        .thenReturn(Instant.EPOCH.plusMillis(2))
        .thenReturn(Instant.EPOCH.plusMillis(3));

    Schedule schedule = new CronSchedule.Builder("* * * * * * * *").preserve(true).build();

    assertEquals(Instant.EPOCH.plusMillis(1), schedule.next(mockedTask));
    sleep(50);
    assertEquals(Instant.EPOCH.plusMillis(2), schedule.next(mockedTask));
    sleep(50);
    assertEquals(Instant.EPOCH.plusMillis(3), schedule.next(mockedTask));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.BaseSchedule#next(edu.byu.hbll.scheduler.ScheduledTask)}.
   */
  @Test
  public void testNextScheduledTaskDelay() {
    Schedule schedule = new OneTimeSchedule.Builder().delay(Duration.ofMillis(100)).build();
    counter = new Counter(0);
    scheduler.scheduleTask(schedule, counter);
    sleep(50);
    assertEquals(0, counter.get());
    sleep(100);
    assertEquals(1, counter.get());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.BaseSchedule#next(edu.byu.hbll.scheduler.ScheduledTask)}.
   */
  @Test
  public void testNextScheduledTaskOffset() {
    Schedule schedule =
        new PeriodicSchedule.Builder(Duration.ofMillis(100)).offset(Duration.ofMillis(50)).build();
    counter = new Counter(0);
    scheduler.scheduleTask(schedule, counter);
    sleep(25); // 25
    assertEquals(0, counter.get());
    sleep(50); // 75
    assertEquals(1, counter.get());
    sleep(50); // 125
    assertEquals(1, counter.get());
    sleep(100); // 225
    assertEquals(2, counter.get());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.BaseSchedule#next(edu.byu.hbll.scheduler.ScheduledTask)}.
   */
  @Test
  public void testNextScheduledTaskJitter() {
    lenient().when(mockedTask.getCreationTime()).thenReturn(Instant.EPOCH);
    lenient().when(mockedTask.getNumScheduled()).thenReturn(0l);

    Schedule schedule = new OneTimeSchedule.Builder().jitter(Duration.ofDays(1)).build();

    long now = Instant.now().truncatedTo(ChronoUnit.SECONDS).toEpochMilli();
    long next1 = schedule.next(mockedTask).truncatedTo(ChronoUnit.SECONDS).toEpochMilli();
    long next2 = schedule.next(mockedTask).truncatedTo(ChronoUnit.SECONDS).toEpochMilli();
    long next3 = schedule.next(mockedTask).truncatedTo(ChronoUnit.SECONDS).toEpochMilli();
    long dayInMillis = Duration.ofDays(1).toMillis();

    // there is a very small possibility that these two are actually equal
    assertNotEquals(now, next1);
    assertNotEquals(now, next2);
    assertNotEquals(now, next3);
    assertNotEquals(next1, next2);
    assertNotEquals(next1, next3);
    assertNotEquals(next2, next3);

    // make sure next with jitter is within hour of exact next
    assertEquals(now, next1, dayInMillis);
    assertEquals(now, next2, dayInMillis);
    assertEquals(now, next3, dayInMillis);
  }

  /** Test method for {@link edu.byu.hbll.scheduler.BaseSchedule#isAsynchronous()}. */
  @Test
  public void testIsAsynchronous() {
    assertFalse(new OneTimeSchedule.Builder().build().isAsynchronous());
    assertTrue(new OneTimeSchedule.Builder().asynchronous(true).build().isAsynchronous());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.BaseSchedule#isPreserve()}. */
  @Test
  public void testIsPreserve() {
    assertFalse(new OneTimeSchedule.Builder().build().isPreserve());
    assertTrue(new OneTimeSchedule.Builder().preserve(true).build().isPreserve());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.BaseSchedule#getDelay()}. */
  @Test
  public void testGetDelay() {
    assertEquals(Duration.ZERO, new OneTimeSchedule.Builder().build().getDelay());
    assertEquals(
        Duration.ofDays(1),
        new OneTimeSchedule.Builder().delay(Duration.ofDays(1)).build().getDelay());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.BaseSchedule#getJitter()}. */
  @Test
  public void testGetJitter() {
    assertEquals(Duration.ZERO, new OneTimeSchedule.Builder().build().getJitter());
    assertEquals(
        Duration.ofDays(1),
        new OneTimeSchedule.Builder().jitter(Duration.ofDays(1)).build().getJitter());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.BaseSchedule#getOffset()}. */
  @Test
  public void testGetOffset() {
    assertEquals(Duration.ZERO, new OneTimeSchedule.Builder().build().getOffset());
    assertEquals(
        Duration.ofDays(1),
        new OneTimeSchedule.Builder().offset(Duration.ofDays(1)).build().getOffset());
  }
}
