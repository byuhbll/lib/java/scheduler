package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.time.Instant;
import org.junit.jupiter.api.Test;

/** {@link ScheduledTask.Builder} also tested here. */
public class ScheduledTaskTest extends SchedulerTesting {

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#cancel()}. */
  @Test
  public void testCancel() {
    counter = new Counter(100);
    task = scheduler.schedulePeriodicTask(Duration.ofMillis(200), counter);
    sleep(50);
    task.cancel();
    sleep(100);
    assertEquals(1, counter.get());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#cancel(boolean)}. */
  @Test
  public void testCancelBoolean() {
    counter = new Counter(100);
    task = scheduler.schedulePeriodicTask(Duration.ofMillis(200), counter);
    sleep(50);
    task.cancel(true);
    sleep(100);
    assertEquals(0, counter.get());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#isCanceled()}. */
  @Test
  public void testIsCanceled() {
    task = scheduler.newOneTimeTask(Duration.ofMillis(100), counter);
    assertFalse(task.isCanceled());
    task.cancel();
    assertTrue(task.isCanceled());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#isDone()}. */
  @Test
  public void testIsDone() {
    task = scheduler.schedulePeriodicTask(Duration.ofMillis(100), counter);
    assertFalse(task.isDone());
    task.cancel();
    sleep(200);
    assertTrue(task.isDone());

    task = scheduler.newOneTimeTask(Duration.ofMillis(100), counter);
    assertFalse(task.isDone());
    sleep(200);
    assertTrue(task.isDone());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getTask()}. */
  @Test
  public void testGetTask() {
    assertEquals(counter, task.getTask());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getNext()}. */
  @Test
  public void testGetNext() {
    sleep(100);
    assertNull(task.getNext());
    task =
        scheduler.scheduleTask(
            new OneTimeSchedule.Builder().offset(Duration.ofSeconds(1)).build(), counter);
    assertEquals(Instant.now().plusSeconds(1).toEpochMilli(), task.getNext().toEpochMilli(), 10);
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getCreationTime()}. */
  @Test
  public void testGetStart() {
    Instant now = Instant.now();
    sleep(10);
    task = scheduler.newOneTimeTask(Duration.ZERO, counter);
    assertTrue(now.isBefore(task.getCreationTime()));
    sleep(10);
    assertTrue(Instant.now().isAfter(task.getCreationTime()));
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getNumExecuted()}. */
  @Test
  public void testGetNumExecuted() {
    sleep(100);
    assertEquals(1, task.getNumExecuted());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getCreationTime()}. */
  @Test
  public void testGetCreationTime() {
    Instant start = Instant.now();
    task = scheduler.newOneTimeTask(Duration.ZERO, counter);
    Instant end = Instant.now();

    assertTrue(start.isBefore(task.getCreationTime()) || start.equals(task.getCreationTime()));
    assertTrue(end.isAfter(task.getCreationTime()) || end.equals(task.getCreationTime()));
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getSchedule()}. */
  @Test
  public void testGetSchedule() {
    assertTrue(task.getSchedule() instanceof OneTimeSchedule);
  }

  /** Test method for {@link edu.byu.hbll.scheduler.ScheduledTask#getSchedule()}. */
  @Test
  public void testSchedule() {
    counter = new Counter(0);
    task = scheduler.newOneTimeTask(Duration.ofDays(1), counter);

    // make sure the specified schedule is honored
    task.schedule(Instant.now());
    sleep(100);
    assertEquals(1, counter.get());

    task.schedule(Instant.now().plusMillis(200));
    assertEquals(1, counter.get());
    sleep(100);
    assertEquals(1, counter.get());
    sleep(200);
    assertEquals(2, counter.get());

    // make sure synchronicity is honored
    counter = new Counter(200);
    task = scheduler.newOneTimeTask(Duration.ZERO, counter);
    task.schedule(Instant.now());
    task.schedule(Instant.now());

    sleep(100);
    assertEquals(0, counter.get());
    sleep(200);
    assertEquals(1, counter.get());
    sleep(200);
    assertEquals(2, counter.get());
    sleep(200);
    assertEquals(3, counter.get());
  }
}
