package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import org.junit.jupiter.api.Test;

public class CronScheduleTest extends SchedulerTesting {

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.CronSchedule#next(edu.byu.hbll.scheduler.ScheduledTask,
   * java.time.Instant, java.time.Instant)}.
   */
  @Test
  public void testNextScheduledTaskInstantInstant() {
    CronSchedule schedule = new CronSchedule.Builder("* * * * * * *").build();
    Instant from = Instant.parse("2000-01-01T00:00:00.000Z");
    assertEquals(from.plusSeconds(1), schedule.next(task, from, from));
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronSchedule#getExpression()}. */
  @Test
  public void testGetExpression() {
    String expression = "* * * * * * *";
    assertEquals(
        expression, new CronSchedule.Builder(expression).build().getExpression().getExpression());
    assertEquals(
        expression,
        new CronSchedule.Builder(new CronExpression(expression))
            .build()
            .getExpression()
            .getExpression());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronSchedule#builder(java.lang.String)}. */
  @Test
  void testBuilderString() {
    assertEquals(
        "* * * * * * *",
        CronSchedule.builder("* * * * * * *").build().getExpression().getExpression());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.CronSchedule#builder(edu.byu.hbll.scheduler.CronExpression)}.
   */
  @Test
  void testBuilderCronExpression() {
    assertEquals(
        "* * * * * * *",
        CronSchedule.builder(new CronExpression("* * * * * * *"))
            .build()
            .getExpression()
            .getExpression());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronSchedule#of(java.lang.String)}. */
  @Test
  void testOfString() {
    assertTrue(CronSchedule.of("* * * * * * *") instanceof CronSchedule);
    assertEquals("* * * * * * *", CronSchedule.of("* * * * * * *").getExpression().getExpression());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.CronSchedule#of(edu.byu.hbll.scheduler.CronExpression)}.
   */
  @Test
  void testOfCronExpression() {
    assertTrue(CronSchedule.of(new CronExpression("* * * * * * *")) instanceof CronSchedule);
    assertEquals(
        "* * * * * * *",
        CronSchedule.of(new CronExpression("* * * * * * *")).getExpression().getExpression());
  }
}
