package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.ZonedDateTime;
import org.junit.jupiter.api.Test;

/** Edge case tests for {@link CronElementValue} are found in {@link CronExpression}. */
public class CronElementValueTest {

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.CronElementValue#matches(java.time.ZonedDateTime)}.
   */
  @Test
  public void testMatches() {
    CronElementValue value = CronElement.DAYS_OF_MONTH.parse("1");
    assertTrue(value.matches(ZonedDateTime.now().withDayOfMonth(1)));
    assertFalse(value.matches(ZonedDateTime.now().withDayOfMonth(2)));
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.CronElementValue#findMatch(java.time.ZonedDateTime, boolean)}.
   */
  @Test
  public void testFindMatch() {
    CronElementValue value = CronElement.DAYS_OF_MONTH.parse("1");
    ZonedDateTime time = ZonedDateTime.now().withDayOfMonth(1);
    assertEquals(time, value.findMatch(time, false).getNext());
    assertFalse(value.findMatch(time, false).isRollover());
    time = time.withDayOfMonth(2);
    assertNotEquals(time, value.findMatch(time, false).getNext());
    assertTrue(value.findMatch(time, false).isRollover());

    value = CronElement.YEARS.parse("*");
    time = ZonedDateTime.now().withYear(10000);
    assertTrue(value.findMatch(time, false).isRollover());
    time = ZonedDateTime.now().withYear(-1);
    assertTrue(value.findMatch(time, true).isRollover());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElementValue#getElement()}. */
  @Test
  public void testGetElement() {
    CronElementValue value = CronElement.DAYS_OF_MONTH.parse("1");
    assertEquals(CronElement.DAYS_OF_MONTH, value.getElement());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElementValue#getExpression()}. */
  @Test
  public void testGetExpression() {
    CronElementValue value = CronElement.DAYS_OF_MONTH.parse("1");
    assertEquals("1", value.getExpression());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElementValue#isStar()}. */
  @Test
  public void testIsStar() {
    assertTrue(CronElement.DAYS_OF_MONTH.parse("*").isStar());
    assertFalse(CronElement.DAYS_OF_MONTH.parse("1").isStar());
  }
}
