package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import org.junit.jupiter.api.Test;

public class CronExpressionTest {

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronExpression#CronExpression(java.lang.String)}.
   */
  @Test
  public void testCronSchedule() {
    new CronExpression("0 0 0 * * * *");
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronSchedule#next()}. */
  @Test
  public void testNext() {
    CronExpression expression = new CronExpression("0 0 0 * * * *");
    assertEquals(expression.next(Instant.now()), expression.next());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronSchedule#nextDelay()}. */
  @Test
  public void testNextDelay() {
    CronExpression expression = new CronExpression("0 0 0 * * * *");
    assertEquals(expression.nextDelay(Instant.now()), expression.nextDelay(), 1);
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronSchedule#nextDelay(java.time.LocalDateTime)}.
   */
  @Test
  public void testNextDelayLocalDateTime() {
    CronExpression expression = new CronExpression("0 0 * * * * *", ZoneId.of("Z"));
    assertEquals(
        59 * 60 * 1000,
        expression.nextDelay(
            ZonedDateTime.of(2000, 1, 1, 1, 1, 0, 0, ZoneId.systemDefault()).toInstant()));
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronSchedule#next(java.time.LocalDateTime)}. Here
   * is where we test all the different scenarios for next.
   */
  @Test
  public void testNextLocalDateTime() {
    // trivial
    testNext("* * * * * * *", "2000-01-01T00:00:00", "2000-01-01T00:00:01");

    // truncate
    testNext("* * * * * * *", "2000-01-01T00:00:00.123", "2000-01-01T00:00:01");

    // all (except dayOfWeek)
    testNext("1 2 3 4 5 * 2006", "2000-01-01T00:00:00", "2006-05-04T03:02:01");

    // rollover
    testNext("0 * * * * * *", "2000-01-01T00:00:00", "2000-01-01T00:01:00");
    testNext("0 0 * * * * *", "2000-01-01T00:00:00", "2000-01-01T01:00:00");
    testNext("0 0 0 * * * *", "2000-01-01T00:00:00", "2000-01-02T00:00:00");
    testNext("0 0 0 1 * * *", "2000-01-01T00:00:00", "2000-02-01T00:00:00");
    testNext("0 0 0 1 1 * *", "2000-01-01T00:00:00", "2001-01-01T00:00:00");

    // dayOfMonth or dayOfWeek must match
    testNext("0 0 0 1 * 0 *", "2000-01-01T00:00:00", "2000-01-02T00:00:00");

    // Sunday is 0 or 7
    testNext("* * * * * 0 *", "2000-01-01T00:00:00", "2000-01-02T00:00:00");
    testNext("* * * * * 7 *", "2000-01-01T00:00:00", "2000-01-02T00:00:00");

    // dayOfWeek doesn't roll over, but month still increments
    testNext("0 0 0 * * 2 *", "2000-01-31T00:00:00", "2000-02-01T00:00:00");

    // near full match followed by full rollover
    testNext(
        "1,998 1,59 1,59 1,23 2,31 2,12 * 1999,2001",
        "1999-12-31T23:59:59.998",
        "2001-02-02T01:01:01.001");

    // no match
    testNext("* * * * * * 1999", "2000-01-01T00:00:00", (String) null);

    // millis
    testNext("* * * * * * * *", "2000-01-01T00:00:00", "2000-01-01T00:00:00.001");
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronSchedule#next(java.time.LocalDateTime)}.
   * Testing parsing of commas.
   */
  @Test
  public void testNextLocalDateTimeComma() {
    testNext(
        "3,5 * * * * * *", "2000-01-01T00:00:00", "2000-01-01T00:00:03", "2000-01-01T00:00:05");
    testNext(
        "0 3,5 * * * * *", "2000-01-01T00:00:00", "2000-01-01T00:03:00", "2000-01-01T00:05:00");
    testNext(
        "0 0 3,5 * * * *", "2000-01-01T00:00:00", "2000-01-01T03:00:00", "2000-01-01T05:00:00");
    testNext(
        "0 0 0 3,5 * * *", "2000-01-01T00:00:00", "2000-01-03T00:00:00", "2000-01-05T00:00:00");
    testNext(
        "0 0 0 * * 3,5 *", "2000-01-01T00:00:00", "2000-01-05T00:00:00", "2000-01-07T00:00:00");
    testNext(
        "0 0 0 1 3,5 * *", "2000-01-01T00:00:00", "2000-03-01T00:00:00", "2000-05-01T00:00:00");
    testNext(
        "0 0 0 1 1 * 2003,2005",
        "2000-01-01T00:00:00",
        "2003-01-01T00:00:00",
        "2005-01-01T00:00:00");
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronSchedule#next(java.time.LocalDateTime)}.
   * Testing parsing of ranges.
   */
  @Test
  public void testNextLocalDateTimeRange() {
    testNext(
        "3-5 * * * * * *",
        "2000-01-01T00:00:00",
        "2000-01-01T00:00:03",
        "2000-01-01T00:00:04",
        "2000-01-01T00:00:05");
    testNext(
        "0 3-5 * * * * *",
        "2000-01-01T00:00:00",
        "2000-01-01T00:03:00",
        "2000-01-01T00:04:00",
        "2000-01-01T00:05:00");
    testNext(
        "0 0 3-5 * * * *",
        "2000-01-01T00:00:00",
        "2000-01-01T03:00:00",
        "2000-01-01T04:00:00",
        "2000-01-01T05:00:00");
    testNext(
        "0 0 0 3-5 * * *",
        "2000-01-01T00:00:00",
        "2000-01-03T00:00:00",
        "2000-01-04T00:00:00",
        "2000-01-05T00:00:00");
    testNext(
        "0 0 0 * * 3-5 *",
        "2000-01-01T00:00:00",
        "2000-01-05T00:00:00",
        "2000-01-06T00:00:00",
        "2000-01-07T00:00:00");
    testNext(
        "0 0 0 1 3-5 * *",
        "2000-01-01T00:00:00",
        "2000-03-01T00:00:00",
        "2000-04-01T00:00:00",
        "2000-05-01T00:00:00");
    testNext(
        "0 0 0 1 1 * 2003-2005",
        "2000-01-01T00:00:00",
        "2003-01-01T00:00:00",
        "2004-01-01T00:00:00",
        "2005-01-01T00:00:00");
  }

  private void testNext(String cronExpression, String from, String... next) {
    CronExpression schedule = new CronExpression(cronExpression, ZoneId.of("Z"));

    for (String expected : next) {
      Instant expectedDate = expected == null ? null : Instant.parse(expected + "Z");
      assertEquals(expectedDate, schedule.next(Instant.parse(from + "Z")));
      from = expected;
    }
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronSchedule#prev()}. */
  @Test
  public void testPrev() {
    CronExpression schedule = new CronExpression("0 0 0 * * * *");
    assertEquals(schedule.prev(Instant.now()), schedule.prev());
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronSchedule#prev(java.time.LocalDateTime)}. Here
   * is where we test all the different scenarios for prev.
   */
  @Test
  public void testPrevLocalDateTime() {
    // trivial
    testPrev("* * * * * * *", "2000-01-01T00:00:01", "2000-01-01T00:00:00");

    // truncate
    testPrev("* * * * * * *", "2000-01-01T00:00:01.123", "2000-01-01T00:00:01");

    // all (except dayOfWeek)
    testPrev("1 2 3 4 5 * 2006", "2007-01-01T00:00:00", "2006-05-04T03:02:01");

    // rollover
    testPrev("0 * * * * * *", "2000-01-01T00:01:00", "2000-01-01T00:00:00");
    testPrev("0 0 * * * * *", "2000-01-01T01:00:00", "2000-01-01T00:00:00");
    testPrev("0 0 0 * * * *", "2000-01-02T00:00:00", "2000-01-01T00:00:00");
    testPrev("0 0 0 1 * * *", "2000-02-01T00:00:00", "2000-01-01T00:00:00");
    testPrev("0 0 0 1 1 * *", "2001-01-01T00:00:00", "2000-01-01T00:00:00");

    // dayOfMonth and dayOfWeek must match
    testPrev("0 0 0 1 * 0 *", "2000-11-01T00:00:00", "2000-10-29T00:00:00");

    // Sunday is 0 or 7
    testPrev("0 0 0 * * 0 *", "2000-01-03T00:00:00", "2000-01-02T00:00:00");
    testPrev("0 0 0 * * 7 *", "2000-01-03T00:00:00", "2000-01-02T00:00:00");

    // dayOfWeek doesn't roll over, but month still decrements
    testPrev("0 0 0 * * 1 *", "2000-02-01T00:00:00", "2000-01-31T00:00:00");

    // near full match followed by full rollover
    testPrev(
        "998,1 1,59 1,59 1,23 2,31 2,12 * 1999,2001",
        "2001-02-02T01:01:01.001",
        "1999-12-31T23:59:59.998");

    // no match
    testPrev("* * * * * * 1999", "1998-01-01T00:00:00", (String) null);

    // millis
    testPrev("* * * * * * * *", "2000-01-01T00:00:00.002", "2000-01-01T00:00:00.001");
  }

  private void testPrev(String cronExpression, String from, String... prev) {
    CronExpression schedule = new CronExpression(cronExpression, ZoneId.of("Z"));

    for (String expected : prev) {
      Instant expectedDate = expected == null ? null : Instant.parse(expected + "Z");
      assertEquals(expectedDate, schedule.prev(Instant.parse(from + "Z")));
      from = expected;
    }
  }

  /**
   * Tests the performance of {@link CronExpression#next(ZonedDateTime)} and {@link
   * CronExpression#prev(ZonedDateTime)}. It should run in several seconds on current hardware.
   */
  @Test
  public void testNextPerformance() {
    CronExpression schedule =
        new CronExpression(
            "19,457,617 "
                + "13,23,37 "
                + "7,31,51 "
                + "1,13,17 "
                + "7,19,23 "
                + "2,7,11 "
                + "1,3,5 "
                + "1977,2001,2023");

    Random r = new Random(0);
    ZonedDateTime millenium = ZonedDateTime.ofInstant(Instant.EPOCH, ZoneId.of("Z")).plusYears(30);
    long thirtyYearsInNanos = ChronoUnit.NANOS.between(millenium, millenium.plusYears(30));

    for (int i = 0; i < 1000000; i++) {
      schedule.next(millenium.plusNanos(r.nextLong() % thirtyYearsInNanos).toInstant());
      schedule.prev(millenium.plusNanos(r.nextLong() % thirtyYearsInNanos).toInstant());
    }
  }
}
