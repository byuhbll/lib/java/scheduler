package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.lenient;

import java.time.Duration;
import java.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class PeriodicScheduleTest {

  @Mock ScheduledTask task;

  @BeforeEach
  public void beforeEach() {
    lenient().when(task.getNumScheduled()).thenReturn(0l).thenReturn(1l);
    lenient().when(task.getCreationTime()).thenReturn(Instant.now());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.PeriodicSchedule#next(edu.byu.hbll.scheduler.ScheduledTask,
   * java.time.Instant, java.time.Instant)}.
   */
  @Test
  public void testNextScheduledTaskInstantInstant() throws Exception {

    Schedule schedule = new PeriodicSchedule.Builder(Duration.ofMillis(100)).build();

    assertEquals(task.getCreationTime(), schedule.next(task));

    Thread.sleep(50);

    Instant expectedNext = Instant.now().plusMillis(100);
    Instant actualNext = schedule.next(task);

    assertTrue(Duration.between(expectedNext, actualNext).compareTo(Duration.ofMillis(10)) < 0);
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.PeriodicSchedule#next(edu.byu.hbll.scheduler.ScheduledTask,
   * java.time.Instant, java.time.Instant)}.
   */
  @Test
  public void testNextScheduledTaskInstantInstantFixedRate() throws Exception {

    Schedule schedule =
        new PeriodicSchedule.Builder(Duration.ofMillis(100)).fixedRate(true).build();

    assertEquals(task.getCreationTime(), schedule.next(task));

    Thread.sleep(50);

    Instant expectedNext = task.getCreationTime().plusMillis(100);
    Instant actualNext = schedule.next(task);

    assertEquals(expectedNext, actualNext);
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#getPeriod()}. */
  @Test
  public void testGetPeriod() {
    assertEquals(
        Duration.ofMillis(100),
        new PeriodicSchedule.Builder(Duration.ofMillis(100)).build().getPeriod());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#isFixedRate()}. */
  @Test
  public void testIsFixedRate() {
    assertTrue(new PeriodicSchedule.Builder(Duration.ZERO).fixedRate(true).build().isFixedRate());
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#builder(java.time.Duration)}.
   */
  @Test
  void testBuilder() {
    assertTrue(PeriodicSchedule.builder(Duration.ofSeconds(1)) instanceof PeriodicSchedule.Builder);
    assertTrue(PeriodicSchedule.builder(Duration.ofSeconds(1)).build() instanceof PeriodicSchedule);
    assertEquals(
        Duration.ofSeconds(1), PeriodicSchedule.builder(Duration.ofSeconds(1)).build().getPeriod());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#of(java.time.Duration)}. */
  @Test
  void testOf() {
    assertTrue(PeriodicSchedule.of(Duration.ofSeconds(1)) instanceof PeriodicSchedule);
    assertEquals(Duration.ofSeconds(1), PeriodicSchedule.of(Duration.ofSeconds(1)).getPeriod());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#ofMillis(long)}. */
  @Test
  void testOfMillis() {
    assertTrue(PeriodicSchedule.ofMillis(1) instanceof PeriodicSchedule);
    assertEquals(Duration.ofMillis(1), PeriodicSchedule.ofMillis(1).getPeriod());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#ofSeconds(long)}. */
  @Test
  void testOfSeconds() {
    assertTrue(PeriodicSchedule.ofSeconds(1) instanceof PeriodicSchedule);
    assertEquals(Duration.ofSeconds(1), PeriodicSchedule.ofSeconds(1).getPeriod());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#ofMinutes(long)}. */
  @Test
  void testOfMinutes() {
    assertTrue(PeriodicSchedule.ofMinutes(1) instanceof PeriodicSchedule);
    assertEquals(Duration.ofMinutes(1), PeriodicSchedule.ofMinutes(1).getPeriod());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.PeriodicSchedule#ofHours(long)}. */
  @Test
  void testOfHours() {
    assertTrue(PeriodicSchedule.ofHours(1) instanceof PeriodicSchedule);
    assertEquals(Duration.ofHours(1), PeriodicSchedule.ofHours(1).getPeriod());
  }
}
