package edu.byu.hbll.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.junit.jupiter.api.Test;

public class SchedulerTest extends SchedulerTesting {

  /** Test method for {@link edu.byu.hbll.scheduler.Scheduler#Scheduler()}. */
  @Test
  public void testScheduler() {
    new Scheduler().shutdown();
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.Scheduler#Scheduler(java.util.concurrent.ThreadFactory)}.
   */
  @Test
  public void testSchedulerThreadFactory() {
    new Scheduler(Executors.defaultThreadFactory()).shutdown();
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.Scheduler#Scheduler(java.util.concurrent.ExecutorService,
   * java.util.concurrent.ThreadFactory)}.
   */
  @Test
  public void testSchedulerExecutorServiceThreadFactory() {
    new Scheduler(Executors.newCachedThreadPool(), Executors.defaultThreadFactory()).shutdown();
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.Scheduler#newOneTimeTask(java.lang.Runnable,
   * java.time.Duration)}.
   */
  @Test
  public void testCreateOneTimeTask() {
    counter = new Counter(0);
    ScheduledTask task = scheduler.newOneTimeTask(Duration.ZERO, counter);
    assertEquals(scheduler, task.getScheduler());
    sleep(100);
    assertEquals(1, counter.get());
    assertNull(task.getNext());
    assertTrue(task.isDone());
  }

  /**
   * Test method for {@link
   * edu.byu.hbll.scheduler.Scheduler#schedulePeriodicTask(java.lang.Runnable, java.time.Duration)}.
   */
  @Test
  public void testCreatePeriodicTask() {
    counter = new Counter(0);
    ScheduledTask task = scheduler.schedulePeriodicTask(Duration.ofMillis(200), counter);
    assertNotNull(task.getNext());
    assertEquals(scheduler, task.getScheduler());
    assertEquals(Duration.ofMillis(200), ((PeriodicSchedule) task.getSchedule()).getPeriod());
    sleep(500);
    assertEquals(3, counter.get());
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.Scheduler#scheduleCronTask(java.lang.Runnable,
   * java.lang.String)}.
   */
  @Test
  public void testCreateCronTaskRunnableString() {
    ScheduledTask task = scheduler.scheduleCronTask("*/100 * * * * * * *", counter);
    assertNotNull(task.getNext());
    assertEquals(scheduler, task.getScheduler());
    assertEquals(
        "*/100 * * * * * * *", ((CronSchedule) task.getSchedule()).getExpression().getExpression());
    sleep(250);
    assertEquals(3, counter.get(), 1);
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.Scheduler#scheduleCronTask(java.lang.Runnable,
   * edu.byu.hbll.scheduler.CronExpression)}.
   */
  @Test
  public void testCreateCronTaskRunnableCronSchedule() {
    ScheduledTask task =
        scheduler.scheduleCronTask(new CronExpression("*/100 * * * * * * *"), counter);
    assertNotNull(task.getNext());
    assertEquals(scheduler, task.getScheduler());
    assertEquals(
        "*/100 * * * * * * *", ((CronSchedule) task.getSchedule()).getExpression().getExpression());
    sleep(250);
    assertEquals(3, counter.get(), 1);
  }

  /** Test method for {@link edu.byu.hbll.scheduler.Scheduler#shutdown()}. */
  @Test
  public void testShutdown() throws Exception {
    counter = new Counter(200);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    sleep(100);
    scheduler.shutdown();
    scheduler.awaitTermination(Duration.ofDays(1));
    assertEquals(2, counter.get());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.Scheduler#shutdownNow()}. */
  @Test
  public void testShutdownExecutor() throws Exception {
    ExecutorService executor = Executors.newCachedThreadPool();
    Scheduler scheduler = new Scheduler(executor, Executors.defaultThreadFactory());
    counter = new Counter(200);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    sleep(100);
    scheduler.shutdown();
    scheduler.awaitTermination(Duration.ofDays(1));
    assertEquals(2, counter.get());
    assertFalse(executor.isShutdown());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.Scheduler#shutdownNow()}. */
  @Test
  public void testShutdownNow() throws Exception {
    counter = new Counter(200);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    sleep(100);
    scheduler.shutdownNow();
    scheduler.awaitTermination(Duration.ofDays(1));
    assertEquals(0, counter.get());
  }

  /** Test method for {@link edu.byu.hbll.scheduler.Scheduler#shutdownNow()}. */
  @Test
  public void testShutdownNowExecutor() throws Exception {
    ExecutorService executor = Executors.newCachedThreadPool();
    Scheduler scheduler = new Scheduler(executor, Executors.defaultThreadFactory());
    counter = new Counter(200);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    sleep(100);
    scheduler.shutdownNow();
    scheduler.awaitTermination(Duration.ofDays(1));
    assertEquals(0, counter.get());
    assertFalse(executor.isShutdown());
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.Scheduler#awaitTermination(java.time.Duration)}.
   */
  @Test
  public void testAwaitTermination() throws Exception {
    counter = new Counter(200);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    sleep(100);
    scheduler.shutdown();
    scheduler.awaitTermination(Duration.ofDays(1));
    assertEquals(2, counter.get());
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.Scheduler#awaitTermination(java.time.Duration)}.
   */
  @Test
  public void testAwaitTerminationNoTasks() throws Exception {
    scheduler.shutdown();
    scheduler.awaitTermination(Duration.ofDays(1));
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.Scheduler#awaitTermination(java.time.Duration)}.
   */
  @Test
  public void testAwaitTerminationTimeout() throws Exception {
    counter = new Counter(200);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    scheduler.schedulePeriodicTask(Duration.ofMillis(1000), counter);
    sleep(100);
    scheduler.shutdown();
    scheduler.awaitTermination(Duration.ofMillis(1));
    assertEquals(0, counter.get());
  }
}
