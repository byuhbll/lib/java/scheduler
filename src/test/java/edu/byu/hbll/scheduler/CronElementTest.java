package edu.byu.hbll.scheduler;

import static edu.byu.hbll.scheduler.CronElement.DAYS_OF_MONTH;
import static edu.byu.hbll.scheduler.CronElement.DAYS_OF_WEEK;
import static edu.byu.hbll.scheduler.CronElement.HOURS;
import static edu.byu.hbll.scheduler.CronElement.MILLIS;
import static edu.byu.hbll.scheduler.CronElement.MINUTES;
import static edu.byu.hbll.scheduler.CronElement.MONTHS;
import static edu.byu.hbll.scheduler.CronElement.SECONDS;
import static edu.byu.hbll.scheduler.CronElement.YEARS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import org.junit.jupiter.api.Test;

public class CronElementTest {

  /** Test method for {@link edu.byu.hbll.scheduler.CronElement#parse(java.lang.String)}. */
  @Test
  public void testParse() {
    testParse(YEARS.parse("2000,2002-2004"), 2000, 2002, 2003, 2004);
    testParse(DAYS_OF_WEEK.parse("1,3-5"), 1, 3, 4, 5);
    testParse(MONTHS.parse("1,3-5"), 1, 3, 4, 5);
    testParse(DAYS_OF_MONTH.parse("1,3-5"), 1, 3, 4, 5);
    testParse(HOURS.parse("1,3-5"), 1, 3, 4, 5);
    testParse(HOURS.parse("*/11"), 0, 11, 22);
    testParse(HOURS.parse("1/11"), 1, 12, 23);
    testParse(MINUTES.parse("1,3-5"), 1, 3, 4, 5);
    testParse(MINUTES.parse("*/11"), 0, 11, 22, 33, 44, 55);
    testParse(MINUTES.parse("1/11"), 1, 12, 23, 34, 45, 56);
    testParse(SECONDS.parse("1,3-5"), 1, 3, 4, 5);
    testParse(SECONDS.parse("*/11"), 0, 11, 22, 33, 44, 55);
    testParse(SECONDS.parse("1/11"), 1, 12, 23, 34, 45, 56);
    testParse(MILLIS.parse("1,3-5"), 1, 3, 4, 5);
    testParse(MILLIS.parse("*/111"), 0, 111, 222, 333, 444, 555, 666, 777, 888, 999);
    testParse(MILLIS.parse("1/111"), 1, 112, 223, 334, 445, 556, 667, 778, 889);

    try {
      YEARS.parse("BAD");
      fail("should throw exception");
    } catch (IllegalArgumentException e) {
    }

    int[] hours = new int[24];

    for (int i = 0; i < 24; i++) {
      hours[i] = i;
    }

    testParse(HOURS.parse("*"), hours);

    assertFalse(HOURS.parse("0").isStar());
    assertTrue(HOURS.parse("*").isStar());
  }

  private void testParse(CronElementValue cron, int... values) {
    int totalMatches = 0;
    ZonedDateTime time = ZonedDateTime.parse("2000-05-01T00:00:00Z");

    for (int i = cron.getElement().minValue(); i <= cron.getElement().maxValue(); i++) {
      totalMatches += cron.matches(time.with(cron.getElement().chronoField(), i)) ? 1 : 0;
    }

    assertEquals(values.length, totalMatches);

    for (int value : values) {
      assertTrue(cron.matches(time.with(cron.getElement().chronoField(), value)));
    }
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElement#minValue()}. */
  @Test
  public void testMinValue() {
    for (CronElement element : CronElement.values()) {
      assertTrue(element.minValue() == 0 || element.minValue() == 1);
    }
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElement#maxValue()}. */
  @Test
  public void testMaxValue() {
    for (CronElement element : CronElement.values()) {
      assertTrue(element.minValue() >= 0);
    }
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElement#chronoField()}. */
  @Test
  public void testChronoField() {
    for (CronElement element : CronElement.values()) {
      assertTrue(element.chronoField() instanceof ChronoField);
    }
  }

  /** Test method for {@link edu.byu.hbll.scheduler.CronElement#chronoUnit()}. */
  @Test
  public void testChronoUnit() {
    for (CronElement element : CronElement.values()) {
      assertTrue(element.chronoUnit() instanceof ChronoUnit);
    }
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronElement#truncate(java.time.ZonedDateTime)}.
   */
  @Test
  public void testTruncate() {
    ZonedDateTime date = ZonedDateTime.parse("2000-02-02T01:01:01.001000001Z");
    assertEquals(ZonedDateTime.parse("2000-01-01T00:00:00.000Z"), YEARS.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-02T00:00:00.000Z"), DAYS_OF_WEEK.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-01T00:00:00.000Z"), MONTHS.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-02T00:00:00.000Z"), DAYS_OF_MONTH.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-02T01:00:00.000Z"), HOURS.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-02T01:01:00.000Z"), MINUTES.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-02T01:01:01.000Z"), SECONDS.truncate(date));
    assertEquals(ZonedDateTime.parse("2000-02-02T01:01:01.001Z"), MILLIS.truncate(date));
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronElement#maxValue(java.time.ZonedDateTime)}.
   */
  @Test
  public void testMaxValueZonedDateTime() {
    ZonedDateTime date = ZonedDateTime.parse("2000-01-01T00:00:00Z");
    assertEquals(9999, YEARS.maxValue(date));
    assertEquals(7, DAYS_OF_WEEK.maxValue(date));
    assertEquals(5, DAYS_OF_WEEK.maxValue(date.minusDays(1)));
    assertEquals(1, DAYS_OF_WEEK.maxValue(date.withDayOfMonth(31)));
    assertEquals(12, MONTHS.maxValue(date));
    assertEquals(31, DAYS_OF_MONTH.maxValue(date));
    assertEquals(29, DAYS_OF_MONTH.maxValue(date.plusMonths(1)));
    assertEquals(30, DAYS_OF_MONTH.maxValue(date.plusMonths(3)));
    assertEquals(23, HOURS.maxValue(date));
    assertEquals(59, MINUTES.maxValue(date));
    assertEquals(59, SECONDS.maxValue(date));
    assertEquals(999, MILLIS.maxValue(date));
  }

  /**
   * Test method for {@link edu.byu.hbll.scheduler.CronElement#minValue(java.time.ZonedDateTime)}.
   */
  @Test
  public void testMinValueZonedDateTime() {
    ZonedDateTime date = ZonedDateTime.parse("2000-01-01T00:00:00Z");
    assertEquals(0, YEARS.minValue(date));
    assertEquals(6, DAYS_OF_WEEK.minValue(date));
    assertEquals(1, DAYS_OF_WEEK.minValue(date.minusDays(1)));
    assertEquals(3, DAYS_OF_WEEK.minValue(date.minusDays(1).withDayOfMonth(1)));
    assertEquals(1, MONTHS.minValue(date));
    assertEquals(1, DAYS_OF_MONTH.minValue(date));
    assertEquals(0, HOURS.minValue(date));
    assertEquals(0, MINUTES.minValue(date));
    assertEquals(0, SECONDS.minValue(date));
    assertEquals(0, MILLIS.minValue(date));
  }
}
