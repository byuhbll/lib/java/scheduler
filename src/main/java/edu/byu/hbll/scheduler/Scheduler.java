package edu.byu.hbll.scheduler;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {@link Scheduler} is similar to Java's {@link ScheduledExecutorService} in that it schedules
 * tasks to be run in their own thread according to some schedule, but also allows cron style
 * scheduling and the ability to supply an {@link ExecutorService} that is not necessarily a {@link
 * ScheduledExecutorService}. It also adds additional scheduling features.
 *
 * <p>The default executor used for running tasks is created by calling {@link
 * Executors#newCachedThreadPool()} and so there is no limit to the number of simultaneous running
 * tasks. Alternatively you may supply your own {@link ExecutorService} that has thread limits.
 *
 * <p>The scheduler is thread-safe and a single instance can be used for an entire application. The
 * scheduling component runs in a single thread, but its sole responsibility is to submit tasks to
 * the {@link ExecutorService} when a timeout occurs so it can handle many scheduled tasks.
 *
 * @author Charles Draper
 */
public class Scheduler {

  private static final Logger logger = Logger.getLogger(Scheduler.class.getName());

  /** The internal timer that schedules running tasks. The tasks themselves do not run here. */
  private final ScheduledExecutorService scheduler;

  /** The executor that executes the tasks. */
  private final ExecutorService executor;

  /** Whether or not the executor was created by this class. */
  private boolean internalExecutor;

  private Set<ScheduledTask> tasks = Collections.synchronizedSet(new LinkedHashSet<>());

  /**
   * Creates a new {@link Scheduler} using {@link Executors#newCachedThreadPool()} as its task
   * executor.
   */
  public Scheduler() {
    this(Executors.defaultThreadFactory());
  }

  /**
   * Creates a new {@link Scheduler} using {@link Executors#newCachedThreadPool()} as its task
   * executor. Threads are created using the provided {@link ThreadFactory}.
   *
   * @param threadFactory the thread factory to be used when creating threads
   */
  public Scheduler(ThreadFactory threadFactory) {
    this(Executors.newCachedThreadPool(threadFactory), threadFactory);
    this.internalExecutor = true;
  }

  /**
   * Creates a new {@link Scheduler} using the given executor as its task executor. The main
   * scheduling thread is created using the provided {@link ThreadFactory}.
   *
   * @param executor the executor to use for executing tasks, ideally this executes tasks in
   *     provided threads so as to not bog down the scheduler thread
   * @param threadFactory the thread factory to be used when creating threads
   */
  public Scheduler(ExecutorService executor, ThreadFactory threadFactory) {
    this.scheduler = Executors.newSingleThreadScheduledExecutor(threadFactory);
    this.executor = executor;
  }

  /**
   * Creates a one time task to run after the initial given delay.
   *
   * @param delay the initial delay
   * @param task the task to schedule
   * @return the configured task
   */
  public ScheduledTask newOneTimeTask(Duration delay, Runnable task) {
    return scheduleTask(new OneTimeSchedule.Builder().delay(delay).build(), task);
  }

  /**
   * Schedules the given task to run periodically according to the given period. Following runs of
   * the task are not scheduled until the task completes. The first run of the task executes
   * immediately. Returns immediately after scheduling the task. See {@link PeriodicSchedule} for
   * more scheduling options.
   *
   * @param period the period of time between runs
   * @param task the task to schedule
   * @return the configured task
   */
  public ScheduledTask schedulePeriodicTask(Duration period, Runnable task) {
    return scheduleTask(new PeriodicSchedule.Builder(period).build(), task);
  }

  /**
   * Schedules the given task according to the given schedule ({@link CronExpression}). If the task
   * runs longer than the scheduled time for the next run, the next run will be dropped. Returns
   * immediately after scheduling the task. See {@link CronSchedule} for more scheduling options.
   *
   * @param cronExpression the schedule
   * @param task the task to schedule
   * @return the configured task
   */
  public ScheduledTask scheduleCronTask(String cronExpression, Runnable task) {
    return scheduleCronTask(new CronExpression(cronExpression), task);
  }

  /**
   * Schedules the given task according to the given schedule. If the task runs longer than the
   * scheduled time for the next run, the next run will be dropped. Returns immediately after
   * scheduling the task. See {@link CronSchedule} for more scheduling options.
   *
   * @param cronExpression the schedule
   * @param task the task to schedule
   * @return the configured task
   */
  public ScheduledTask scheduleCronTask(CronExpression cronExpression, Runnable task) {
    return scheduleTask(new CronSchedule.Builder(cronExpression).build(), task);
  }

  /**
   * Schedules the given task according to the given schedule. Returns immediately after scheduling
   * the task.
   *
   * @param schedule the schedule
   * @param task the task to schedule
   * @return the configured task
   */
  public ScheduledTask scheduleTask(Schedule schedule, Runnable task) {
    return new ScheduledTask(this, schedule, task);
  }

  /**
   * Schedules the given task to run at the next calculated run time according to the task's
   * settings. Returns immediately after scheduling the task.
   *
   * @param task the task to schedule
   * @return the configured task
   * @throws IllegalStateException if the task is already scheduled to run in another scheduler
   */
  void schedule(ScheduledTask.Instance task, Instant timeout) {
    // calculate the amount of time between now and the next scheduled run in millis
    long delay = Instant.now().until(timeout, ChronoUnit.MILLIS);

    try {
      // schedule the task to run at `delay` milliseconds from now
      scheduler.schedule(() -> submit(task), delay, TimeUnit.MILLISECONDS);
    } catch (Exception e) {
      logger.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  /**
   * Submits the task to the executor.
   *
   * @param task the task to run
   */
  private void submit(ScheduledTask.Instance task) {
    try {
      task.setFuture(executor.submit(task));
    } catch (Exception e) {
      logger.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  /**
   * A scheduled task adds itself here upon scheduling.
   *
   * @param task the scheduled task
   */
  void addTask(ScheduledTask task) {
    tasks.add(task);
  }

  /**
   * A scheduled task removes itself here upon canceling.
   *
   * @param task the scheduled task
   */
  void removeTask(ScheduledTask task) {
    tasks.remove(task);
  }

  /**
   * Terminates this scheduler and discards any scheduled tasks. Currently executing and previously
   * submitted tasks to the {@link ExecutorService} are allowed to finish.
   *
   * <p>Note: this does not shutdown a supplied executor.
   */
  public void shutdown() {
    for (ScheduledTask task : new ArrayList<>(tasks)) {
      task.cancel(false);
    }

    scheduler.shutdownNow();

    if (internalExecutor) {
      ((ExecutorService) executor).shutdown();
    }
  }

  /**
   * Terminates this scheduler and discards any scheduled tasks. Attempts to stop all actively
   * executing tasks and halts the processing of waiting tasks.
   *
   * <p>There are no guarantees beyond best-effort attempts to stop processing actively executing
   * tasks. For example, typical implementations will cancel via {@link Thread#interrupt}, so any
   * task that fails to respond to interrupts may never terminate.
   *
   * <p>Note: this does not shutdown a supplied executor, but does attempt to cancel the scheduled
   * tasks executing in the supplied executor.
   */
  public void shutdownNow() {
    for (ScheduledTask task : new ArrayList<>(tasks)) {
      task.cancel(true);
    }

    scheduler.shutdownNow();

    if (internalExecutor) {
      ((ExecutorService) executor).shutdownNow();
    }
  }

  /**
   * Blocks until all tasks have completed execution after a shutdown request, or the timeout
   * occurs, or the current thread is interrupted, whichever happens first.
   *
   * @param timeout the maximum time to wait
   * @return {@code true} if all tasks terminated and {@code false} if the timeout elapsed before
   *     termination
   * @throws InterruptedException if interrupted while waiting
   */
  public boolean awaitTermination(Duration timeout) throws InterruptedException {
    boolean success = scheduler.awaitTermination(timeout.toMillis(), TimeUnit.MILLISECONDS);

    if (!success) {
      return false;
    }

    if (internalExecutor) {
      return executor.awaitTermination(timeout.toMillis(), TimeUnit.MILLISECONDS);
    } else {
      Instant until = Instant.now().plus(timeout);

      while (!tasks.isEmpty() && until.isAfter(Instant.now())) {
        Thread.sleep(100);
      }
    }

    return true;
  }
}
