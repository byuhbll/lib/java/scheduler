package edu.byu.hbll.scheduler;

import edu.byu.hbll.scheduler.CronElementValue.Match;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * A parsed cron expression. The cron expression follows the Java EE 7 Schedule specification found
 * at https://docs.oracle.com/javaee/7/api/javax/ejb/Schedule.html with three exceptions. First, in
 * dayOfMonth, only the numeric values [1,31] are implemented. Calculated days based on the end of
 * month or the day of week occurrence are not implemented. Second, this allows for millisecond
 * resolution. If there are eight elements in the expression instead of seven, the first clause is
 * parsed as millis [0,999]. Finally, whitespace should only occur in separating elements.
 * Whitespace is not valid within an element.
 *
 * @author Charles Draper
 */
public class CronExpression {

  /** Standard number of elements in cron expression. */
  private static final int NUM_ELEMENTS = 7;

  /** Expanded number of elements with millis. */
  private static final int NUM_ELEMENTS_WITH_MILLIS = 8;

  /** The supplied cron expression. */
  private final String expression;

  /** The supplied time zone or system default if none supplied. */
  private final ZoneId zone;

  /** Complete list of Unit enum values. */
  private final List<CronElement> units;

  /** Enumerated values for each unit according to the cron expression. */
  private final Map<CronElement, CronElementValue> values = new LinkedHashMap<>();

  /**
   * Creates a new {@link CronExpression} with the given expression and uses the system default
   * zone.
   *
   * @param expression the expression to parse, see class level documentation for format.
   * @throws IllegalArgumentException if unable to parse expression
   */
  public CronExpression(String expression) {
    this(expression, ZoneId.systemDefault());
  }

  /**
   * Creates a new {@link CronExpression} with the given expression and zone.
   *
   * @param expression the expression to parse, see class level documentation for format.
   * @param zone time zone to use
   * @throws IllegalArgumentException if unable to parse expression
   */
  public CronExpression(String expression, ZoneId zone) {
    this.expression = expression;
    this.zone = zone;

    // split on white space and create mutable list
    List<String> elements = new LinkedList<>(Arrays.asList(expression.split("\\s+")));

    if (elements.size() != NUM_ELEMENTS && elements.size() != NUM_ELEMENTS_WITH_MILLIS) {
      throw new IllegalArgumentException(
          "incorrect number of elements, expecting 7 or 8 (including millis)");
    }

    // if millis are not specified add default 0
    if (elements.size() == NUM_ELEMENTS) {
      elements.add(0, "0");
    }

    // parse each element and store in values
    for (int i = 0; i < elements.size(); i++) {
      String element = elements.get(i);
      CronElement unit = CronElement.values()[i];
      values.put(unit, unit.parse(element));
    }

    List<CronElement> units = new ArrayList<>(Arrays.asList(CronElement.values()));

    // days of week is checked at the same time as days of month
    units.remove(CronElement.DAYS_OF_WEEK);

    this.units = Collections.unmodifiableList(units);
  }

  /**
   * Returns the next scheduled time from now.
   *
   * @return next scheduled time from now
   */
  public Instant next() {
    return next(Instant.now());
  }

  /**
   * Returns the next scheduled time from the given time.
   *
   * @param from next is based on this time
   * @return next scheduled time from this time
   */
  public Instant next(Instant from) {
    return next(from, false);
  }

  /**
   * Same as {@link #next(ZonedDateTime, boolean)}, but first converts {@link Instant} to {@link
   * ZonedDateTime}.
   *
   * @param from next is based on this time
   * @param reverse false for next, true for previous
   * @return scheduled time after or before this time
   */
  private Instant next(Instant from, boolean reverse) {
    ZonedDateTime next = next(from.atZone(zone), reverse);
    return next == null ? null : next.toInstant();
  }

  /**
   * Calculates next or previous scheduled.
   *
   * @param from next is based on this time
   * @param reverse false for next, true for previous
   * @return next scheduled time after or before this time
   */
  private ZonedDateTime next(ZonedDateTime from, boolean reverse) {
    // truncates time to milliseconds
    ZonedDateTime next = from.truncatedTo(ChronoUnit.MILLIS);

    if (reverse) {
      // subtract 1 millisecond for prev time unless truncation had an effect
      next = next.equals(from) ? from.minus(1, ChronoUnit.MILLIS) : next;
    } else {
      // add one millisecond to bump to next time
      next = next.plus(1, ChronoUnit.MILLIS);
    }

    // stack of units to check for acceptance
    Stack<CronElement> units = new Stack<>();
    units.addAll(this.units);

    // check each unit to see if next is acceptable
    // loop from largest unit to smallest
    // we have found the next acceptable time if no more units to check
    while (!units.isEmpty()) {
      // unit to check
      CronElement unit = units.pop();

      // get parsed acceptable values
      CronElementValue value = values.get(unit);

      // find next acceptable value for this unit
      Match nextMatch = value.findMatch(next, reverse);

      // must do days of week/month together
      if (unit == CronElement.DAYS_OF_MONTH) {
        // get parsed acceptable values for days of week
        CronElementValue value2 = values.get(CronElement.DAYS_OF_WEEK);

        // get next acceptable value for days of week
        Match nextMatch2 = value2.findMatch(next, reverse);

        // if both days of month and days of week are not starred, then logic is OR
        if (!value.isStar() && !value2.isStar()) {
          // find closest day either by day of month or day of week
          if (reverse && nextMatch2.getNext().isAfter(nextMatch.getNext())) {
            nextMatch = nextMatch2;
          } else if (!reverse && nextMatch2.getNext().isBefore(nextMatch.getNext())) {
            nextMatch = nextMatch2;
          }
        } else if (!value2.isStar()) {
          // use day of week match if day of month is star and day of week is not
          nextMatch = nextMatch2;
        }
      }

      // if current unit rolled over and caused a larger unit to change
      if (nextMatch.isRollover()) {
        // return null if reached the end; YEAR < 0 or YEAR > 9999
        if (unit == CronElement.YEARS) {
          return null;
        } else {
          // go back to top and try again (1 ms could have caused all units to change)
          units.clear();
          units.addAll(this.units);
        }
      }

      next = nextMatch.getNext();
    }

    return next;
  }

  /**
   * Returns the delay between now and the next scheduled time in milliseconds.
   *
   * @return delay between now and next scheduled time in milliseconds
   */
  public long nextDelay() {
    return nextDelay(Instant.now());
  }

  /**
   * Returns the delay between the given time and the next scheduled time in milliseconds.
   *
   * @param from next is based on this time
   * @return delay between from and next scheduled time in milliseconds
   */
  public long nextDelay(Instant from) {
    long delay = ChronoUnit.MILLIS.between(from, next(from));
    return delay;
  }

  /**
   * Returns the previous scheduled time before now.
   *
   * @return previous scheduled time before now
   */
  public Instant prev() {
    return prev(Instant.now());
  }

  /**
   * Returns the previous scheduled time before the given time.
   *
   * @param from prev is based on this time
   * @return previous scheduled time before this time
   */
  public Instant prev(Instant from) {
    return next(from, true);
  }

  /**
   * Returns the supplied cron expression.
   *
   * @return supplied cron expression
   */
  public String getExpression() {
    return expression;
  }

  /**
   * Returns the supplied time zone or system default if not specified.
   *
   * @return supplied time zone or system default if not specified
   */
  public ZoneId getZone() {
    return zone;
  }
}
