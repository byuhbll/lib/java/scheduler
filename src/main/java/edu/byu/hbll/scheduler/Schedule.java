package edu.byu.hbll.scheduler;

import java.time.Instant;

/**
 * A schedule to be used by the {@link Scheduler} when scheduling tasks. The schedule defines the
 * next time a task should run when the scheduler attempts to schedule the next run. Note that the
 * scheduler calls {@link #next} immediately upon submitting the schedule and task to the scheduler.
 * All subsequent calls to {@link #next} will occur immediately after running the task currently for
 * synchronous tasks or immediately before running the task currently for asynchronous tasks.
 *
 * @author Charles Draper
 */
public interface Schedule {

  /**
   * Returns the next scheduled time the task should run. Assumptions should be made that proper
   * scheduling and synchronization of tasks have taken place and it's time to determine when the
   * task should run again based on the task that just started (asynchronous) or just finished
   * (synchronous). Because this method is called in real time, all time calculations should be
   * based on the current time.
   *
   * @param task the current state of the task as some schedules will need this additional
   *     information
   * @return next the next scheduled time greater than or equal to now
   */
  Instant next(ScheduledTask task);

  /**
   * Whether or not executions of a task can run concurrently. If true, scheduling of the next be
   * determined at the start of each task. If false, scheduling of the next will be determined right
   * after each task finishes. Default is false.
   *
   * @return whether or not executions of a task can run concurrently
   */
  default boolean isAsynchronous() {
    return false;
  }
}
