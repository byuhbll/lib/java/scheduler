package edu.byu.hbll.scheduler;

import java.time.YearMonth;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents an element in a cron expression. See
 * https://docs.oracle.com/javaee/7/api/javax/ejb/Schedule.html.
 *
 * @author Charles Draper
 */
public enum CronElement {
  /** Milliseconds. */
  MILLIS(
      0,
      999,
      ChronoField.MILLI_OF_SECOND,
      ChronoUnit.MILLIS,
      ChronoUnit.MILLIS,
      "(\\*|0*[0-9]?[0-9]?[0-9]?[0-9])(/0*[0-9]?[0-9]?[0-9]?[0-9])?"),

  /** Seconds. */
  SECONDS(
      0,
      59,
      ChronoField.SECOND_OF_MINUTE,
      ChronoUnit.SECONDS,
      ChronoUnit.SECONDS,
      "(\\*|0*[1-5]?[0-9])(/0*[1-5]?[0-9])?"),

  /** Minutes. */
  MINUTES(
      0,
      59,
      ChronoField.MINUTE_OF_HOUR,
      ChronoUnit.MINUTES,
      ChronoUnit.MINUTES,
      "(\\*|0*[1-5]?[0-9])(/0*[1-5]?[0-9])?"),

  /** Hours. */
  HOURS(
      0,
      23,
      ChronoField.HOUR_OF_DAY,
      ChronoUnit.HOURS,
      ChronoUnit.HOURS,
      "(\\*|0*(1?[0-9]|2[0-3]))(/1?[0-9]|2[0-3])?"),

  /** Days of the month. */
  DAYS_OF_MONTH(
      1,
      31,
      ChronoField.DAY_OF_MONTH,
      ChronoUnit.DAYS,
      ChronoUnit.DAYS,
      "0*([1-9]|[1-2][0-9]|3[0-1])|\\*"),

  /** Months. */
  MONTHS(
      1,
      12,
      ChronoField.MONTH_OF_YEAR,
      ChronoUnit.MONTHS,
      ChronoUnit.DAYS,
      "0*([1-9]|1[0-2])|(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)|\\*"),

  /** Days of the week. */
  DAYS_OF_WEEK(
      1,
      7,
      ChronoField.DAY_OF_WEEK,
      ChronoUnit.DAYS,
      ChronoUnit.DAYS,
      "0*[0-7]|(sun|mon|tue|wed|thu|fri|sat)|\\*"),

  /** Years. */
  YEARS(
      0,
      9999,
      ChronoField.YEAR,
      ChronoUnit.YEARS,
      ChronoUnit.DAYS,
      "0*[0-9]?[0-9]?[0-9]?[0-9]|\\*");

  /** Spelled out months and days mapped to their equivalent integer values. */
  private static final Map<String, Integer> translations = new HashMap<>();

  /** The lowest acceptable value for this element. */
  private final int minValue;

  /** The highest acceptable value for this element. */
  private final int maxValue;

  /** The corresponding {@link ChronoField} for this element. */
  private final ChronoField chronoField;

  /** The corresponding {@link ChronoUnit} for this element. */
  private final ChronoUnit chronoUnit;

  /** Truncate to this {@link ChronoUnit} when looping. */
  private final ChronoUnit truncate;

  /** Regex that validates the element in the cron expression. */
  private final String validation;

  static {
    // initialize translations (sun appears twice because of 0,7)
    String[] days = "sun,mon,tue,wed,thu,fri,sat,sun".split(",");
    String[] months = "jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec".split(",");

    for (int i = 0; i < days.length; i++) {
      translations.put(days[i], i);
    }

    for (int i = 0; i < months.length; i++) {
      translations.put(months[i], i);
    }
  }

  /**
   * Creates a new {@link CronElement}.
   *
   * @param minValue lowest acceptable value for this unit
   * @param maxValue highest acceptable value for this unit
   * @param chronoField {@link ChronoField} representing this unit
   * @param increment increment by this {@link ChronoUnit} when looping
   * @param truncate truncate to this {@link ChronoUnit} when looping
   * @param validation regex that validates the element in the cron expression
   */
  private CronElement(
      int minValue,
      int maxValue,
      ChronoField chronoField,
      ChronoUnit increment,
      ChronoUnit truncate,
      String validation) {
    this.minValue = minValue;
    this.maxValue = maxValue;
    this.chronoField = chronoField;
    this.chronoUnit = increment;
    this.truncate = truncate;
    this.validation = validation;
  }

  /**
   * Translate string value to corresponding integer.
   *
   * @param value value to translate
   * @return the integer representation
   */
  private int translate(String value) {
    return translations.getOrDefault(value, Integer.parseInt(value));
  }

  /**
   * Parses an individual element from a cron expression and returns an enumerated array of
   * acceptable values.
   *
   * @param element the string to parse
   * @return enumerated array of all acceptable values for this element
   */
  public CronElementValue parse(String element) {
    boolean[] values = new boolean[maxValue + 1];
    boolean star = false;

    // split on commas as this denotes a list of values
    for (String val : element.split(",")) {

      // validation
      for (String part : val.split("-")) {
        if (!part.matches(validation)) {
          throw new IllegalArgumentException(
              "`" + val + "` not valid expression in " + name().toLowerCase() + " element");
        }
      }

      // * enables all values
      if (val.equals("*")) {
        for (int i = minValue; i <= maxValue; i++) {
          values[i] = true;
        }

        star = true;
      } else if (val.contains("-")) {
        // split on - for ranges
        String[] range = val.split("-");

        // get lower and upper bound of range
        int lower = translate(range[0]);
        int upper = translate(range[1]);

        // enable values in the range
        for (int i = lower; i <= upper; i++) {
          values[i] = true;
        }
      } else if (val.contains("/")) {
        // increments
        String[] parts = val.split("/");
        int x = parts[0].equals("*") ? 0 : Integer.parseInt(parts[0]);
        int y = Integer.parseInt(parts[1]);

        for (int i = x; i < values.length; i += y) {
          values[i] = true;
        }
      } else {
        // enable specific value
        values[translate(val)] = true;
      }
    }

    // for days of week, 0 and 7 are both Sunday, but here only 7 is Sunday
    if (this == DAYS_OF_WEEK) {
      values[7] |= values[0];
    }

    CronElementValue value = new CronElementValue(this, element, values, star);

    return value;
  }

  /**
   * Returns the corresponding {@link ChronoField} for this element.
   *
   * @return the corresponding {@link ChronoField} for this element
   */
  public ChronoField chronoField() {
    return chronoField;
  }

  /**
   * Returns the corresponding {@link ChronoUnit} for this element.
   *
   * @return the corresponding {@link ChronoUnit} for this element
   */
  public ChronoUnit chronoUnit() {
    return chronoUnit;
  }

  /**
   * Truncates given date to the corresponding {@link ChronoUnit} for this element.
   *
   * @param dateTime the date to truncate
   * @return the truncated dateTime
   */
  public ZonedDateTime truncate(ZonedDateTime dateTime) {
    // truncate to this unit
    dateTime = dateTime.truncatedTo(truncate);

    // special cases because year and months can't be truncated
    if (this == YEARS) {
      // truncate to first day of the year
      dateTime = dateTime.withDayOfYear(1);
    } else if (this == MONTHS) {
      // truncate to first day of the month
      dateTime = dateTime.withDayOfMonth(1);
    }

    return dateTime;
  }

  /**
   * Returns the highest acceptable value for this element.
   *
   * @return the highest acceptable value for this element
   */
  public int maxValue() {
    return maxValue;
  }

  /**
   * Finds the max value for the corresponding {@link ChronoField} within the context of the given
   * date without causing any other unit to rollover (ie, if week straddles a month or year only
   * accept days that don't cause those to increment).
   *
   * @param dateTime the context
   * @return the maximum value
   */
  public int maxValue(ZonedDateTime dateTime) {
    if (this == DAYS_OF_WEEK) {
      int month = dateTime.getMonthValue();

      for (int i = dateTime.get(chronoField); i < maxValue; i++) {
        dateTime = dateTime.plusDays(1);

        if (month != dateTime.getMonthValue()) {
          return i;
        }
      }

      return maxValue;
    } else if (this == DAYS_OF_MONTH) {
      YearMonth month = YearMonth.from(dateTime);
      return month.lengthOfMonth();
    } else {
      return maxValue;
    }
  }

  /**
   * Returns the lowest acceptable value for this element.
   *
   * @return the lowest acceptable value for this element
   */
  public int minValue() {
    return minValue;
  }

  /**
   * Finds the min value for the corresponding {@link ChronoField} within the context of the given
   * date without causing any other unit to rollover (ie, if week straddles a month or year only
   * accept days that don't cause those to decrement).
   *
   * @param dateTime the context
   * @return the minimum value
   */
  public int minValue(ZonedDateTime dateTime) {
    if (this == DAYS_OF_WEEK) {
      int month = dateTime.getMonthValue();

      for (int i = dateTime.get(chronoField); i > minValue; i--) {
        dateTime = dateTime.minusDays(1);

        if (month != dateTime.getMonthValue()) {
          return i;
        }
      }

      return minValue;
    } else {
      return minValue;
    }
  }
}
