package edu.byu.hbll.scheduler;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import lombok.Getter;

/**
 * An abstract {@link Schedule} with various settings and adjustments built in.
 *
 * @author Charles Draper
 */
public abstract class BaseSchedule implements Schedule {

  private final Random random = new Random();

  /** Whether or not task executions can run concurrently. */
  protected final boolean asynchronous;

  /** Whether or not timeouts should be preserved rather than tossed. */
  @Getter protected final boolean preserve;

  /** The initial delay before first run of the task. */
  @Getter protected final Duration delay;

  /** Adds random delay between -jitter and +jitter to the next scheduled execution times. */
  @Getter protected final Duration jitter;

  /** Adds a fixed offset to the next scheduled execution time. */
  @Getter protected final Duration offset;

  /**
   * Creates a new {@link BaseSchedule} from the builder.
   *
   * @param builder the builder
   */
  protected BaseSchedule(Builder<?> builder) {
    this.asynchronous = builder.asynchronous;
    this.preserve = builder.preserve;
    this.delay = builder.delay;
    this.jitter = builder.jitter;
    this.offset = builder.offset;
  }

  @Override
  public Instant next(ScheduledTask task) {
    // true start time
    Instant start = task.getCreationTime().plus(delay);

    // determine from what time next calculations should be based
    Instant from = Instant.now();

    // if preserve, base time on last scheduled time
    from = preserve ? task.getNumScheduled() == 0 ? start : task.getNext() : from;

    // make sure from is not set before start
    from = from.isBefore(start) ? start : from;

    // get next scheduled time
    Instant next = next(task, start, from);

    if (next != null) {
      // apply the offset
      next = next.plus(offset);

      // apply jitter if non-zero
      if (!jitter.isZero()) {
        next = next.plus(Duration.ofMillis(random.nextLong() % jitter.toMillis()));
      }
    }

    return next;
  }

  /**
   * Same as {@link #next(ScheduledTask)}, but with additional parameters for proper calculations.
   * Other adjustments to the next time such as jitter and offset will be made after this call.
   *
   * @param task the current state of the task as some schedules will need this additional
   *     information
   * @param start the true start time after the initial delay. The start time is equivalent to
   *     task.getCreationTime().plus(delay). Calculations depending on creationTime should use start
   *     instead.
   * @param from use this instead of the current time when calculating next. At this point from has
   *     taken into account the initial delay, synchronization, and preservation.
   * @return the next scheduled time
   */
  protected abstract Instant next(ScheduledTask task, Instant start, Instant from);

  @Override
  public boolean isAsynchronous() {
    return asynchronous;
  }

  /**
   * The builder for {@link BaseSchedule}.
   *
   * @author Charles Draper
   */
  public abstract static class Builder<T extends Builder<T>> {

    private boolean asynchronous;
    private boolean preserve;
    private Duration delay = Duration.ZERO;
    private Duration jitter = Duration.ZERO;
    private Duration offset = Duration.ZERO;

    /**
     * Sets the initial delay before first run of the task. For cron schedules, the next run will be
     * calculated after this delay (default: 0).
     *
     * @param delay the initial delay
     * @return this
     */
    public T delay(Duration delay) {
      this.delay = delay;
      return getThis();
    }

    /**
     * Sets whether or not subsequent executions are allowed to start before the current execution
     * finishes. See {@link Schedule#isAsynchronous()}. (default: false)
     *
     * @param asynchronous whether or not task executions can run concurrently
     * @return this
     */
    public T asynchronous(boolean asynchronous) {
      this.asynchronous = asynchronous;
      return getThis();
    }

    /**
     * Adds random delay between -jitter and +jitter to the next scheduled execution times.
     * (default: 0)
     *
     * @param jitter additional random jitter
     * @return this
     */
    public T jitter(Duration jitter) {
      this.jitter = jitter;
      return getThis();
    }

    /**
     * Adds a fixed offset to the next scheduled execution time. (default: 0)
     *
     * @param offset additional time offset
     * @return this
     */
    public T offset(Duration offset) {
      this.offset = offset;
      return getThis();
    }

    /**
     * Sets whether or not to preserve timeouts if missed due to long running execution causing two
     * or more executions to occur in rapid succession to "catch up". Preserve is not applicable if
     * asynchronous is set to true. (default: false)
     *
     * @param preserve whether or not to preserve timeouts
     * @return this
     */
    public T preserve(boolean preserve) {
      this.preserve = preserve;
      return getThis();
    }

    /**
     * Return this without an unchecked cast warning.
     *
     * @return this
     */
    protected abstract T getThis();

    /**
     * Build the {@link Schedule}.
     *
     * @return the build schedule
     */
    public abstract BaseSchedule build();
  }
}
