package edu.byu.hbll.scheduler;

import java.time.Instant;

/**
 * A schedule based on a {@link CronExpression}.
 *
 * @author Charles Draper
 */
public class CronSchedule extends BaseSchedule {

  /** The underlying cron expression for this schedule. */
  protected final CronExpression expression;

  private CronSchedule(Builder builder) {
    super(builder);
    this.expression = builder.expression;
  }

  /**
   * Creates a new builder with the given cron expression (see {@link CronExpression}).
   *
   * @param expression the cron expression
   * @return the builder
   */
  public static CronSchedule.Builder builder(String expression) {
    return new CronSchedule.Builder(expression);
  }

  /**
   * Creates a new builder with the given cron expression.
   *
   * @param expression the cron expression
   * @return the builder
   */
  public static CronSchedule.Builder builder(CronExpression expression) {
    return new CronSchedule.Builder(expression);
  }

  /**
   * Creates a new schedule based on the cron expression.
   *
   * @param expression the cron expression
   * @return a new schedule based on the cron expression.
   */
  public static CronSchedule of(String expression) {
    return builder(expression).build();
  }

  /**
   * Creates a new schedule based on the cron expression.
   *
   * @param expression the cron expression
   * @return a new schedule based on the cron expression.
   */
  public static CronSchedule of(CronExpression expression) {
    return builder(expression).build();
  }

  @Override
  protected Instant next(ScheduledTask task, Instant start, Instant from) {
    Instant next = expression.next(from);
    return next;
  }

  /**
   * Returns the underlying cron expression.
   *
   * @return the cron expression
   */
  public CronExpression getExpression() {
    return expression;
  }

  /**
   * Builder for {@link CronSchedule}.
   *
   * @author Charles Draper
   */
  public static class Builder extends BaseSchedule.Builder<Builder> {

    private CronExpression expression;

    /**
     * Creates a new builder with the given cron expression.
     *
     * @see CronExpression
     * @param expression the cron expression
     */
    public Builder(String expression) {
      this.expression = new CronExpression(expression);
    }

    /**
     * Creates a new builder with the given cron expression.
     *
     * @param expression the cron expression
     */
    public Builder(CronExpression expression) {
      this.expression = expression;
    }

    @Override
    public CronSchedule build() {
      return new CronSchedule(this);
    }

    @Override
    protected Builder getThis() {
      return this;
    }
  }
}
