package edu.byu.hbll.scheduler;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;

/**
 * A parsed cron element housing the acceptable values for the element.
 *
 * @author Charles Draper
 */
public class CronElementValue {

  private CronElement element;
  private String expression;
  private boolean[] values;
  private boolean star;
  private int min;
  private int max;

  CronElementValue(CronElement element, String expression, boolean[] values, boolean star) {
    this.element = element;
    this.expression = expression;
    this.values = values;
    this.star = star;

    this.min = Integer.MAX_VALUE;
    this.max = Integer.MIN_VALUE;

    for (int i = 0; i < values.length; i++) {
      if (values[i]) {
        this.min = Math.min(this.min, i);
        this.max = Math.max(this.max, i);
      }
    }
  }

  /**
   * Tests whether or not the supplied time is allowed for this element only.
   *
   * @param dateTime the supplied time
   * @return whether or not it is allowed
   */
  public boolean matches(ZonedDateTime dateTime) {
    return values[getValue(dateTime)];
  }

  /**
   * Returns the numeric value of this element for the supplied time.
   *
   * @param dateTime the supplied time
   * @return the numeric value of this element for the supplied time
   */
  private int getValue(ZonedDateTime dateTime) {
    return dateTime.get(element.chronoField());
  }

  /**
   * Finds next matching time for this element only (inclusive). If no matching time is found
   * without rolling over or causing another unit to change (days of month/week considered same
   * unit), rolls this unit over and indicates in the response that a rollover has occurred.
   *
   * @param dateTime from this time
   * @param reverse whether or not to go backward in time
   * @return the next match
   */
  public Match findMatch(ZonedDateTime dateTime, boolean reverse) {
    Match match = new Match();
    match.next = dateTime;

    int current = getValue(dateTime);

    // if time outside cron parameters (ie, year is too far in past or future)
    if (current > element.maxValue() || current < element.minValue()) {
      match.rollover = true;
      return match;
    } else if (values[current]) {
      // if current value is acceptable, return match
      return match;
    }

    // get min and max values for this unit that would not cause a rollover
    int contextMin = element.minValue(dateTime);
    int contextMax = element.maxValue(dateTime);

    int matchMin = Math.max(contextMin, this.min);
    int matchMax = Math.min(contextMax, this.max);

    // reverse direction for previous vs next
    int dir = reverse ? -1 : 1;

    int start = reverse ? Math.min(current, matchMax) : Math.max(current, matchMin);

    // loop through acceptable values starting with current and not going past min or max
    for (int i = start; i >= matchMin && i <= matchMax; i += dir) {
      // if acceptable value found
      if (values[i]) {
        // if any amount of looping took place, smaller units will need to be truncated away
        match.next = element.truncate(dateTime.with(element.chronoField(), i));

        if (reverse) {
          // necessary adjustment when finding previous time vs next time
          match.next = match.next.plus(1, element.chronoUnit()).minus(1, ChronoUnit.MILLIS);
        }

        return match;
      }
    }

    // if not match found, force rollover
    if (reverse) {
      match.next =
          element
              .truncate(dateTime)
              .with(element.chronoField(), contextMin)
              .minus(1, ChronoUnit.MILLIS);
    } else {
      match.next =
          element
              .truncate(dateTime)
              .with(element.chronoField(), contextMax)
              .plus(1, element.chronoUnit());
    }

    match.rollover = true;

    return match;
  }

  /**
   * Returns the element.
   *
   * @return the element
   */
  public CronElement getElement() {
    return element;
  }

  /**
   * Returns the original cron expression for the element.
   *
   * @return the original cron expression for the element
   */
  public String getExpression() {
    return expression;
  }

  /**
   * Returns whether or not the cron expression was a star.
   *
   * @return whether or not the cron expression was a star
   */
  public boolean isStar() {
    return star;
  }

  /**
   * Represents the next acceptable time and if no acceptable time found, the next rolled over time
   * with rollover indicated.
   *
   * @author Charles Draper
   */
  public static class Match {
    private ZonedDateTime next;
    private boolean rollover;

    private Match() {}

    /**
     * Creates a new {@link Match} with the given next time and rollover flag.
     *
     * @param next the next acceptable time, or if none, the rolled over time
     * @param rollover hether or not a rollover occurred
     */
    public Match(ZonedDateTime next, boolean rollover) {
      this.next = next;
      this.rollover = rollover;
    }

    /**
     * Returns the next acceptable time, or if none, the rolled over time.
     *
     * @return the next acceptable time, or if none, the rolled over time
     */
    public ZonedDateTime getNext() {
      return next;
    }

    /**
     * Returns whether or not a rollover occurred.
     *
     * @return whether or not a rollover occurred
     */
    public boolean isRollover() {
      return rollover;
    }

    @Override
    public String toString() {
      return next + " " + rollover;
    }
  }
}
