package edu.byu.hbll.scheduler;

import java.time.Instant;

/**
 * A schedule that only runs once.
 *
 * @author Charles Draper
 */
public class OneTimeSchedule extends BaseSchedule {

  private OneTimeSchedule(Builder builder) {
    super(builder);
  }

  /**
   * Creates a new builder.
   *
   * @return the builder
   */
  public static OneTimeSchedule.Builder builder() {
    return new OneTimeSchedule.Builder();
  }

  @Override
  public Instant next(ScheduledTask task, Instant start, Instant from) {
    if (task.getNumScheduled() > 0) {
      return null;
    } else {
      return from;
    }
  }

  /**
   * Builder for {@link OneTimeSchedule}.
   *
   * @author Charles Draper
   */
  public static class Builder extends BaseSchedule.Builder<Builder> {

    @Override
    public OneTimeSchedule build() {
      return new OneTimeSchedule(this);
    }

    @Override
    protected Builder getThis() {
      return this;
    }
  }
}
