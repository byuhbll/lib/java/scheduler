package edu.byu.hbll.scheduler;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Represents a task to be run by {@link Scheduler} and its corresponding schedule.
 *
 * @author Charles Draper
 */
public class ScheduledTask {

  private final Scheduler scheduler;

  private final Runnable task;
  private final Schedule schedule;
  private final Instant creationTime;

  private final Set<Instance> instances = Collections.synchronizedSet(new LinkedHashSet<>());
  private final ReentrantLock oneOffLock = new ReentrantLock();

  private volatile Instant next;

  private volatile boolean canceled;

  private AtomicLong numExecuted = new AtomicLong();
  private AtomicLong numScheduled = new AtomicLong();

  /**
   * Creates a task to run once.
   *
   * @param scheduler the scheduler to run this task
   * @param task the task to execute
   */
  ScheduledTask(Scheduler scheduler, Schedule schedule, Runnable task) {
    this.scheduler = scheduler;
    this.schedule = schedule;
    this.task = task;
    this.creationTime = Instant.now();
    scheduler.addTask(this);
    scheduleNext();
  }

  /** Schedules the next execution of the task with the scheduler. */
  private synchronized void scheduleNext() {
    next = canceled ? null : schedule.next(this);

    if (next != null) {
      schedule(next, false);
    }
  }

  /**
   * Schedule a one-time execution of the task with the scheduler. The internal schedule of the task
   * will proceed as normal, but synchronization of the task is honored.
   *
   * @param timeout when the task should run
   */
  public synchronized void schedule(Instant timeout) {
    schedule(timeout, true);
  }

  /**
   * Schedule a one-time execution of the task with the scheduler. The internal schedule of the task
   * will proceed as normal, but synchronization of the task is honored.
   *
   * @param timeout when the task should run
   */
  private synchronized void schedule(Instant timeout, boolean oneOff) {
    numScheduled.incrementAndGet();
    Instance instance = new Instance(oneOff);
    instances.add(instance);
    scheduler.schedule(instance, timeout);
  }

  /**
   * Cancels this cron task. If the task is running when this call occurs, the task will run to
   * completion, but will never run again. No-op if the task is already canceled.
   *
   * @return this
   */
  public synchronized ScheduledTask cancel() {
    return cancel(false);
  }

  /**
   * Cancels this cron task. If the task has already started, then the {@code mayInterruptIfRunning}
   * parameter determines whether the thread executing this task should be interrupted in an attempt
   * to stop the task. No-op if the task is already canceled.
   *
   * @param mayInterruptIfRunning {@code true} if the thread executing this task should be
   *     interrupted; otherwise, in-progress tasks are allowed to complete
   * @return this
   */
  public synchronized ScheduledTask cancel(boolean mayInterruptIfRunning) {
    if (!canceled) {
      this.canceled = true;
      this.next = null;

      for (Instance instance : new ArrayList<>(instances)) {
        if (instance.future != null) {
          instance.future.cancel(mayInterruptIfRunning);

          if (!instance.started) {
            instances.remove(instance);
          }
        }
      }

      if (isDone()) {
        scheduler.removeTask(this);
      }
    }

    return this;
  }

  /**
   * Returns the scheduler of this task.
   *
   * @return the scheduler of this task
   */
  public Scheduler getScheduler() {
    return scheduler;
  }

  /**
   * Returns whether or not this task has been canceled.
   *
   * @return whether or not this task has been canceled
   */
  public boolean isCanceled() {
    return canceled;
  }

  /**
   * Returns whether or not this task is done and will never run again.
   *
   * @return whether or not this task is done and will never run again
   */
  public boolean isDone() {
    return next == null && instances.isEmpty();
  }

  /**
   * Return the task.
   *
   * @return the task
   */
  public Runnable getTask() {
    return task;
  }

  /**
   * Returns the schedule governing when this task will run.
   *
   * @return the schedule governing when this task will run
   */
  public Schedule getSchedule() {
    return schedule;
  }

  /**
   * Returns the next time the task will start including the currently running task.
   *
   * @return the next time the task will start including the currently running task
   */
  public Instant getNext() {
    return next;
  }

  /**
   * Returns the creation time of the scheduled task. The scheduling of the first run of the task
   * happens at this time.
   *
   * @return the creation time
   */
  public Instant getCreationTime() {
    return creationTime;
  }

  /**
   * Returns how many times the task has been scheduled to run.
   *
   * @return how many times the task has been scheduled to run
   */
  public long getNumScheduled() {
    return numScheduled.get();
  }

  /**
   * Returns how many times the task has executed including those currently running.
   *
   * @return how many times the task has executed including those currently running
   */
  public long getNumExecuted() {
    return numExecuted.get();
  }

  /**
   * Represents a single execution of this {@link ScheduledTask}.
   *
   * @author Charles Draper
   */
  class Instance implements Runnable {
    private final CountDownLatch futureSetLatch = new CountDownLatch(1);
    private final boolean oneOff;
    private volatile Future<?> future;
    private volatile boolean started;

    /**
     * Creates new instance.
     *
     * @param oneOff whether or not this is considered a one off execution of the task and is
     *     therefore not part of the internal schedule
     */
    Instance(boolean oneOff) {
      this.oneOff = oneOff;
    }

    @Override
    public void run() {
      try {
        if (!schedule.isAsynchronous()) {
          // lock in the case of one-offs
          oneOffLock.lock();
        }

        started = true;

        // do not proceed until the future has been set
        futureSetLatch.await();

        // run if not canceled
        if (!canceled) {
          // increment the total runs
          numExecuted.incrementAndGet();

          // schedule next timeout now if asynchronous
          if (schedule.isAsynchronous() && !oneOff) {
            scheduleNext();
          }

          // run the task
          task.run();
        }
      } catch (InterruptedException e) {
        return;
      } finally {
        instances.remove(this);

        if (isDone()) {
          scheduler.removeTask(ScheduledTask.this);
        }

        oneOffLock.unlock();

        if (!schedule.isAsynchronous() && !oneOff) {
          // schedule next timeout now if synchronous
          scheduleNext();
        }
      }
    }

    /**
     * Set the future so that it can be canceled if cancel is called.
     *
     * @param future the future for the execution
     */
    void setFuture(Future<?> future) {
      this.future = Objects.requireNonNull(future);
      futureSetLatch.countDown();
    }
  }
}
